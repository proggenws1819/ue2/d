/**
 * Diese Klasse modelliert eine Adresse
 *
 * @author Cedrico Knoesel
 * @since 05.12.2018
 */
public class Address {
    private String streetName;
    private String houseNumber; //String da z.B. 42a eine Hausnummer ist
    private String postalCode; //String da z.B. Kanada Buchstaben in ihren Postleitzahlen haben
    private String city;

    public Address(String streetName, String houseNumber, String postalCode, String city) {
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
    }

    //Getter Methoden
    public String getStreetName() {
        return streetName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    //Setter-Methoden
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return streetName + " " + houseNumber + ", " + postalCode + " " + city;
    }
}
