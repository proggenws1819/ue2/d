/**
 * Diese Klasse modelliert eine Datumsangabe mit Tag, Monat und Jahr
 *
 * @author Cedrico Knoesel
 * @since 05.12.2018
 */
public class Date {
    private int day;
    private int month;
    private int year;

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    //Getter-Methoden
    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    //Setter-Methoden
    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    /**
     * erzeugt einen String im Format Tag.Monat.Jahr und gibt ihn zurück.
     * Dieses Format wurde auf Grund des Deutschen Standards gewählt.
     *
     * @return Datum im Format Tag.Monat.Jahr
     */
    @Override
    public String toString() {
        return String.format("%02d.%02d.%d", day, month, year);
    }
}
