/**
 * Diese Klasse modelliert einen Film aus einer Filmdatenbank
 *
 * @author Cedrico Knoesel
 * @since 05.12.2018
 */
public class Movie {
    private String title;
    private int duration; //in Minuten
    private Date releaseDate;
    private Genre genre;
    private int id;

    private static int idCounter = 0;

    /**
     * Konstruktor der Klasse Movie
     *
     * @param title    Titel des Films
     * @param duration Spiellänge des Films in Minuten
     * @param day      Tag der Erscheinung
     * @param month    Monat der Erscheinung
     * @param year     Jahr der Erscheinung
     * @param genre    Genre des Films
     */
    public Movie(String title, int duration, int day, int month, int year, String genre) {
        this.title = title;
        this.duration = duration;
        this.releaseDate = new Date(day, month, year);
        this.setGenre(genre);
        this.id = Movie.idCounter++;
    }

    /**
     * Zweiter Konstrukter der Klasse Movie, bei dem das Datum direkt übergeben wird und der Enum-Wert des Genres
     * anstatt des Strings
     *
     * @param title       Titel des Films
     * @param duration    Spiellänge in Minuten
     * @param releaseDate Erscheinungsdatum des Films als Date
     * @param genre       Genre des Films
     * @see Date
     * @see Genre
     */
    public Movie(String title, int duration, Date releaseDate, Genre genre) {
        this.title = title;
        this.duration = duration;
        this.releaseDate = releaseDate;
        this.setGenre(genre);
        this.id = Movie.idCounter++;
    }

    //Getter Methoden
    public String getTitle() {
        return title;
    }

    public int getDuration() {
        return duration;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public Genre getGenre() {
        return genre;
    }

    public int getId() {
        return id;
    }

    //Setter Methoden

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    /**
     * Alternative Setter-Methode des Genres, bei dem das Genre mit der deutschen Bezeichnung übergeben werden kann.
     * Danach wird der entsprechende Enum-Wert dem eigenen Attribut zugewiesen.
     *
     * @param genre Genre des Films
     */
    public void setGenre(String genre) {
        //Das Genre kann als String übergeben und wird nun dem entsprechenden Enum zugeordnet, damit die
        // Vergleichbarkeit im späteren besser gegeben ist.
        switch (genre) {
            case "Aktion":
                this.genre = Genre.ACTION;
                break;
            case "Horror":
                this.genre = Genre.HORROR;
                break;
            case "Science-Fiction":
                this.genre = Genre.SCIENCE_FICTION;
                break;
            case "Komoedie":
                this.genre = Genre.COMEDY;
                break;
            case "Thriller":
                this.genre = Genre.THRILLER;
                break;
            case "Western":
                this.genre = Genre.WESTERN;
                break;
            case "Abenteuer":
                this.genre = Genre.ADVENTURE;
                break;
            default:
                this.genre = Genre.DEFAULT;
        }
    }

    /**
     * Erzeugt einen String der alle Informationen eines Films enthält. Format:
     * [id]: [title], Länge: [duration] min, Erscheinungsdatum [dd.mm.yyyy], Genre: [genre]
     * Diese Ausgabe wurde gewählt, da durch die id am Anfang eine Ordnung entsteht, wenn mehrere Filme angezeigt
     * werden und ansonsten werden alle Informationen möglichst knapp dargestellt.
     *
     * @return Darstellung des Films als String
     */
    @Override
    public String toString() {
        return String.format("%03d: %s, Länge: %d min, Erscheinungsdatum: %s, Genre: %s", id, title, duration,
                releaseDate.toString(), genre.toString());
    }
}
