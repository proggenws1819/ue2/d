/**
 * Diese Klasse modelliert einen Schauspieler aus einer Filmdatenbank
 *
 * @author Cedrico Knoesel
 * @since 05.12.2018
 */
public class Actor {
    private Movie[] movies;
    private String name;
    private Date birthday;
    private Address address;
    private int id;

    private static int idCounter;

    /**
     * Konstruktor der Klasse Actor, der ein Geburtstag im Date-Format erlaubt
     *
     * @param movies   Liste an Filmen in denen der Schauspieler mitspielt
     * @param name     Name des Schauspielers
     * @param birthday Geburtsdatum des Schauspielers
     * @param address  Adresse
     * @see Date
     */
    public Actor(Movie[] movies, String name, Date birthday, Address address) {
        this.movies = movies;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
    }

    /**
     * Konstruktor der Klasse Actor, die ein Geburtstag durch Eingabe einzelner Werte für Tag, Monat und Jahr erlaubt
     *
     * @param movies  Liste an Filmen in denen der Schauspieler mitspielt
     * @param name    Name des Schauspielers
     * @param bDay    Geburtstag des Schauspielers
     * @param bMonth  Geburtsmonat des Schauspielers
     * @param bYear   Geburtsjahr des Schauspielers
     * @param address Adresse
     */
    public Actor(Movie[] movies, String name, int bDay, int bMonth, int bYear, Address address) {
        this.movies = movies;
        this.name = name;
        this.birthday = new Date(bDay, bMonth, bYear);
        this.address = address;
    }

    //Getter Methoden
    public Movie[] getMovies() {
        return movies;
    }

    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public Address getAddress() {
        return address;
    }

    public int getId() {
        return id;
    }

    //Setter Methoden

    public void setMovies(Movie[] movies) {
        this.movies = movies;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * fügt einen Film dem entsprechenden Schauspieler hinzu.
     * Die Größe des Arrays wird dabei angepasst, sodass kein IndexOutOfBounce Error entstehen kann
     *
     * @param movie neuer hinzuzufügender Film
     */
    public void addMovie(Movie movie) {
        Movie[] newMovies = new Movie[this.movies.length + 1];
        System.arraycopy(this.movies, 0, newMovies, 0, this.movies.length);
        newMovies[newMovies.length - 1] = movie;
        this.movies = newMovies;
    }

    /**
     * Entfernt einen Film aus den Filmen eines Schauspielers.
     * Dabei wird wie bei addMovie(Movie movie) das Array angepasst, sodass this.movies.length genau gleich der
     * Anzahl der Filme bleibt.
     * Hinweis: Es wird ein Film nur gelöscht, wenn die Referenz auf das gleiche Objekt
     * eines der Filme übergeben wird.
     *
     * @param movie Referenz auf den zu löschenden Film
     * @return Wahrheitswert, ob löschen erfolgreich war
     */
    public boolean delMovie(Movie movie) {
        int delId = -1;
        for (int i = 0; i < this.movies.length; i++) {
            if (this.movies[i] == movie) {
                delId = i;
            }
        }
        if (delId < 0) {
            return false;
        }
        Movie[] newMovies = new Movie[this.movies.length - 1];
        System.arraycopy(this.movies, 0, newMovies, 0, delId);
        System.arraycopy(this.movies, delId + 1, newMovies, delId, this.movies.length - (delId + 1));
        this.movies = newMovies;
        return true;
    }

    /**
     * Schreibt alle relevaten Informationen eines Schauspielers in einen String.
     * Zuerst die persönlichen Information (Name (Geburtsdatum) Addresse)
     * gefolgt von einer eingerückten Liste aller Filme.
     * Die Einrückungen wurden verwendet um die Übersichtlichkeit
     * bei der Ausgabe mehrerer Schauspieler zu erhöhen.
     *
     * @return Informationen eines Schauspielers in einem mehrzeiligen String.
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(String.format("%s (%s) %s \n\tFilme:", this.name,
                this.birthday.toString(), this.address.toString()));
        for (Movie movie : this.movies) {
            builder.append("\n\t\t").append(movie.toString());
        }
        return builder.toString();
    }
}
