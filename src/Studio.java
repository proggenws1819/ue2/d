/**
 * Diese Klasse modelliert einen Film aus einer Filmdatenbank
 *
 * @author Cedrico Knoesel
 * @since 05.12.2018
 */
public class Studio {
    private String name;
    private Address address;

    public Studio(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Studio: " + name + ", " + address.toString();
    }
}
