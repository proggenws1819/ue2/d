public enum Genre {
    ACTION,
    HORROR,
    SCIENCE_FICTION,
    COMEDY,
    THRILLER,
    WESTERN,
    ADVENTURE,
    DEFAULT;

    public String toString() {
        switch (this) {
            case ACTION:
                return "Aktion";
            case HORROR:
                return "Horror";
            case SCIENCE_FICTION:
                return "Science-Fiction";
            case COMEDY:
                return "Komoedie";
            case THRILLER:
                return "Thriller";
            case WESTERN:
                return "Western";
            case ADVENTURE:
                return "Abenteuer";
            default:
                return "Default";
        }
    }
}
