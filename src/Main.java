public class Main {

    public static void main(String[] args) {
        Movie m1 = new Movie("Ein RUPPiges Fest", 69, new Date(13, 12, 1780), Genre.HORROR);
        Movie m2 = new Movie("Fröhliche Worschnachten", 1337, 1, 1, 2012, "Komoedie");
        Movie m3 = new Movie("Herzog, der Fröhliche", 90, 21, 11, 2111, "Aktion");
        Movie m4 = new Movie("Leonid, der Ehrenmann", 1337, new Date(4, 8, 2016), Genre.WESTERN);
        System.out.println(m1);
        System.out.println(m2);

        Movie[] movies = {m3, m4};
        Actor a1 = new Actor(movies, "Student", new Date(24, 12, 1945), new Address("Prokrastinationsgasse", "1337",
                "hdf67", "City of Exmatrikulation"));
        System.out.println(a1);
        a1.addMovie(m1);
        System.out.println(a1);
        a1.setName("Super Student");
        a1.addMovie(m2);
        System.out.println(a1);
        a1.delMovie(m3);
        System.out.println(a1);

        System.out.println(new Studio("Rupps Büro", new Address("Hintergasse", "96", "===>", "Asstown")));
    }
}
